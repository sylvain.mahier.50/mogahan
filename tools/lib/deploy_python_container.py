import re
import subprocess

# TODO factorize common parts with deploy_web_app.py

def deploy_python_container(staging: bool):
    if is_repo_dirty():
        input('WARNING: repo is dirty. Press Ctrl-C to abort, or Enter to continue.')

    branch = get_current_branch()

    env_file = 'staging.env' if staging else 'prod.env'

    compose_project_name = 'mogahan_staging' if staging else 'mogahan'

    subprocess.run(
        'ssh root@hetzner-vps'.split(),
        text=True,
        input='\n'.join([
            'set -o errexit',
            'set -o verbose',
            'cd repos/mogahan',
            'git fetch',
            f'git checkout --force {branch}',
            f'git reset --hard origin/{branch}',
            'CURRENT_COMMIT=`git log -1 --format="%h"`',
            ' '.join([
                'docker compose',
                f'-p {compose_project_name}',
                '--file docker-compose.yml',
                f'--env-file {env_file}',
                'build',
                '--build-arg version=$CURRENT_COMMIT',
                'python',
            ]),
            ' '.join([
                'docker compose',
                f'-p {compose_project_name}',
                '--file docker-compose.yml',
                f'--env-file {env_file}',
                'up',
                '--force-recreate',
                '--detach',
                'python',
            ]),
        ])
    )


def is_repo_dirty():
    proc = subprocess.run(
        'git status --short'.split(),
        capture_output=True,
        check=True,
    )
    out = proc.stdout

    if len(out) > 0:
        return True
    else:
        return False


BRANCH_HEAD_REGEXP = re.compile(r'^ref: refs/heads/(?P<refname>[a-zA-Z0-9-/]+)$')

def get_current_branch():
    with open('.git/HEAD') as f:
        head = f.read().strip()

    match = BRANCH_HEAD_REGEXP.match(head)
    
    if not match:
        raise Exception(f'git head does not point to a branch: {head}')

    branch = match.group('refname')

    return branch