#!/usr/bin/env bash
source dev.env

echo go to: http://$WEB_APP_SITE

docker compose \
  --file docker-compose.yml \
  --file dev.docker-compose.yml \
  --env-file dev.env \
  up --build