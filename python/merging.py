from collections import namedtuple
from dataclasses import dataclass
import logging
from typing import NamedTuple

delimiter = """
def chercher(recherche):
    resultats = list()
""".strip()

search_return = ' '*4 + 'return resultats'

def split_save(source: str):
    data, code_with_return = source.split(delimiter)

    code, _ = code_with_return.split(search_return)

    return data.strip('\n'), code.strip('\n')

StudentId = str

class Section(NamedTuple):
    student_id: StudentId
    source: str

class Merger:
    def __init__(self):
        self.data_sections = list[Section]()
        self.code_sections = list[Section]()

    def add_save(self, student_id: StudentId, source: str):
        data, code = split_save(source)
        
        self.data_sections.append(Section(student_id, data))
        self.code_sections.append(Section(student_id, code))

    def merge(self):
        # TODO use Jinja2 instead?

        result = str()

        for s in self.data_sections:
            for each in [
                f'# session élève {s.student_id}',
                s.source,
            ]:
                result += each
                result += '\n\n'

        result += '\n'
        result +=  delimiter
        result += '\n\n'

        for s in self.code_sections:
            for each in [
                ' '*4 + f'# session élève {s.student_id}',
                s.source,
            ]:
                result += each
                result += '\n\n'

        result += search_return

        return result

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    merger = Merger()
    for i, save in enumerate(['save_1.py', 'save_2.py'], start=1):
        logging.info(save)
        with open(save) as f:
            source = f.read()

            merger.add_save(source=source, student_id=StudentId(i))


    with open('merged.py', 'w') as f:
        f.write(merger.merge())


