from flask import Flask, jsonify, request

import merging

app = Flask(__name__)

@app.route("/merge", methods=['POST'])
def merge_saves():
    merger = merging.Merger()

    if not request.json:
        return jsonify(msg='no JSON'), 400

    for student_id, source in request.json['saves'].items():
        merger.add_save(student_id, source)

    return jsonify(merged=merger.merge())
