# session élève 1

mots_cles_1 = ["cartographie", "antiquité", "moyen âge"]
mots_cles_2 = ["cartographie", "moyen âge", "renaissance"]

# session élève 2

document_1 = dict()
document_1["mots_cles"] = ["cartographie", "python"]
document_1["adresse"] = "https://scitools.org.uk/cartopy/docs/latest/"

document_2 = dict()
document_2["mots_cles"] = ["cartographie", 'SQL', 'SQLite']
document_2["adresse"] = "https://www.gaia-gis.it/fossil/libspatialite/index"


def chercher(recherche):
    resultats = list()

    # session élève 1

    if recherche in mots_cles_1: resultats.append("https://www.franceculture.fr/emissions/le-cours-de-l-histoire/le-cours-de-l-histoire-emission-du-lundi-06-septembre-2021")
    if recherche in mots_cles_2: resultats.append("https://www.franceculture.fr/emissions/le-cours-de-l-histoire/le-cours-de-l-histoire-emission-du-mardi-07-septembre-2021")

    # session élève 2

    for document in [document_1, document_2]:
        if recherche in document["mots_cles"]:
            resultats.append(document["adresse"])

    return resultats