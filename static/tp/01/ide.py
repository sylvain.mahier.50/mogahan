import sys
import inspect

import tb as traceback
from browser import document, html, console as js_console, window
from my_interpreter import Interpreter, editor_ns
from searchEngine import compute_result_item, bindSearch

console = document["console-textarea"]
webapp_stderr_overlay = document.select('#webapp-pane .stderr')[0]
webapp_export_button = document.select('#webapp-pane #export-btn')[0]

interpreter = None
# used to preserve interpreter history between relaunch
interpreter_history = list()

class cOutput:
    encoding = 'utf-8'

    def __init__(self):
        self.cons = console
        self.buf = ''

    def write(self, data):
        self.buf += str(data)

    def flush(self):
        self.cons.value += self.buf
        self.buf = ''

    def __len__(self):
        return len(self.buf)

cOut = cOutput()


class WebAppErrorOutput:
    # adding this because it's in `class cOutput`
    encoding = 'utf-8'

    def write(self, data):
        webapp_stderr_overlay <= str(data)

web_app_error_output = WebAppErrorOutput()


def run_to_console(src):
    # XXX for an unknown reason, if we remove this instruction,
    # printing at the top level of the editor file does not print any more
    cOut.write('')

    sys.stdout = cOut
    sys.stderr = cOut

    global interpreter
    
    try:
        if interpreter:
            document['console-textarea'].value += '\n=== REDÉMARRAGE ===\n'

        ns = {'__name__':'__main__'}
        exec(src, ns)
        if interpreter:
            ns.update(editor_ns)
            interpreter.globals = ns
            interpreter.locals = ns
            interpreter._status = 'main'
            interpreter.buffer = ''
            interpreter.current = 0
            document['console-textarea'].value += '>>> '
        else:
            interpreter = Interpreter(
                elt_id='console-textarea', globals=ns,
                clear_zone=False, banner=False,
                default_css=False
            )
        # restoring history from previous executions
        # XXX definitely a hack;
        # also note that we have to restore the consistency of the "current" attribute
        interpreter.history = interpreter_history
        interpreter.current = len(interpreter.history)
    except Exception as exc:
        traceback.print_exc(file=sys.stderr)

    sys.stdout.flush()
    console.scrollTop = console.scrollHeight

def build_cmd(namespace):
    if 'chercher' not in namespace or not inspect.isfunction(namespace['chercher']):
        raise Exception('impossible de trouver une fonction nommée "chercher"')

    params = list(inspect.signature(namespace['chercher']).parameters.keys())

    if params == ['recherche']:
        cmd = lambda query: f'chercher("""{query}""")'
    # XXX unused (we removed the part where students would write a pure function)
    elif params == ['recherche', 'toutes_les_ressources']:
        if 'toutes_les_ressources' not in namespace:
            raise Exception('impossible de trouver une variable nommée "toutes_les_ressources"')
        cmd = lambda query: f'chercher("""{query}""", toutes_les_ressources)'
    else:
        raise Exception(
            'la fonction "chercher" n\'a pas les arguments attendus: '
            '"chercher(recherche)" ou "chercher(recherche, toutes_les_ressources)"'
        )

    return cmd

def display_web_app_restart_badge():
    webapp_navbar = document.select('#webapp-pane .navbar')[0]
    badge = html.SPAN('redémarrage…', Class="badge bg-light text-dark")
    webapp_navbar <= badge
    window.setTimeout(lambda: webapp_navbar.removeChild(badge), 500)

def run_to_webapp(source):
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__

    webapp_stderr_overlay.clear()
    webapp_stderr_overlay.style['display'] = 'none'

    webapp_export_button.style['display'] = 'none'

    document['search-results'].clear()
    document['search-input'].value = ''

    if document['webapp'].style['display'] == 'none':
        document.select('#webapp-pane .overlay.not-started')[0].style['display'] = 'none'

    search_form = document.select('#webapp-pane #search-form')[0]
    search_form.unbind()

    display_web_app_restart_badge()
    
    namespace = {'__name__':'__main__'}
    try:
        exec(source, namespace)
        cmd = build_cmd(namespace)
    except Exception as error:
        web_app_error_output.write("ERREUR DURANT LE CHARGEMENT DU CODE DE L'ÉDITEUR : \n\n")
        traceback.print_exc(web_app_error_output)
        webapp_stderr_overlay.style['display'] = None
        document['webapp'].style['display'] = 'none'
        return

    bindSearch(lambda query: eval(cmd(query), namespace, namespace))

    webapp_export_button.style['display'] = None
    document['webapp'].style['display'] = None

# `bind` gives `run` some arguments even if we don't really want them
def run(*args, **kwargs):
    source = document["editor-textarea"].value

    if document.select('#webapp-pane')[0].style['display'] == 'none':
        run_to_console(source)
    else:
        run_to_webapp(source)

console.value = ''
document['run-btn'].bind('click', run)
run_to_console(src="")
