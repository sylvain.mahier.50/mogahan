// renderDocList(allDocs);
// renderIndex(computeStats());

document.getElementById('search-form').onsubmit = (event) => {
    event.preventDefault();

    const data = new FormData(event.target);
    const query = data.get('query');

    console.log()

    const result = search(query);

    renderSearchResults(result);
}