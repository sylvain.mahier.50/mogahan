// To run:
// npx ts-node --esm ./create-token.ts ["account info"]

import { createAccountCreationToken } from './src/lib/database.js';

const info = process.argv[2];

const token = createAccountCreationToken(info);

console.log(`/register#token=${token}`);