export const errorMsgs = {
    accountCreationTokenInvalid: 'le lien de création de compte est invalide',
    accountCreationTokenExpired: 'le lien de création de compte est expiré',
}