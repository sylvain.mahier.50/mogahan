import * as BetterSQLite3 from "better-sqlite3";
import type { DateTime } from "luxon";
import * as argon2 from "argon2";

import { building } from '$app/environment';
import { env } from "$env/dynamic/private";

import { randomBase64, randomHex } from "./cryptography.js";
import type { Account, AccountCreationToken, LabSession, LabStudentSession,  } from "./types.js";
import { Forbidden } from "./catchForbiddenErrors.js";

let database: BetterSQLite3.Database;
if (!building) {
    const pathToDB = `${env.ROOT_PATH || ""}/var/lib/mogahan/database.db`

    try {
        database = BetterSQLite3.default(pathToDB);
    } catch (error) {
        throw new Error(`openning database at "${pathToDB}": ${error}`);
    }

    // TODO use instead a DB migration management tool
    // (the JavaScript equivalent of https://github.com/pressly/goose?)
    // to check the database is what we think it is
    const tables = database.prepare(`
        SELECT name FROM sqlite_schema
        WHERE type ='table' AND name NOT LIKE 'sqlite_%';`
    ).all();
    if (tables.length == 0) {
        throw new Error(`database "${pathToDB}" seems empty (zero tables)`)
    }
}

// TODO split this file, it's getting too big

export function getTeacherLabSessions(teacherId: string, authenticated: AuthenticatedAccount) {
    if (teacherId != authenticated.account.id) {
        throw new Forbidden();
    }

    const rows = database.prepare(`
        SELECT * FROM lab_sessions
        WHERE teacher_account_id = ?
    `).all(teacherId) as LabSession[];

    return rows;
}

export function createLabSession({
    title, notesForTeacher, notesForStudents,  authenticated
}:{
    title: string,
    notesForTeacher: string | null,
    notesForStudents: string | null,
    authenticated: AuthenticatedAccount,
}) {
    // TODO move this check to SQLite itself?
    if (!title) {
        throw new Error("title must not be empty");
    }

    const id = randomHex(16);
    const createdAt = (new Date()).toISOString();

    const result = database.prepare(`
        INSERT INTO lab_sessions(
            id, title, created_at, teacher_account_id,
            notes_for_teacher, notes_for_students
        ) VALUES (?, ?, ?, ?, ?, ?)
    `).run(
        id, title, createdAt, authenticated.account.id,
        notesForTeacher, notesForStudents,
    );

    if (result.changes != 1) {
        throw new Error(`expected 1 row affected, got ${result.changes}`);
    }

    return id;
}

export function getLabSession(id: string, authenticated: AuthenticatedAccount) {
    const labSession = database.prepare(`
        SELECT 
            id, title, created_at,
            teacher_account_id, join_code, joinable_until,
            notes_for_teacher, notes_for_students
        FROM lab_sessions
        WHERE id = ?
    `).get(id) as (LabSession & {teacher_account_id: string}) | undefined;

    // XXX inneficient: many functions call this function first
    // and apply their own access control after.
    // Ideally we should decouple data retrieval from access control

    if (!labSession) {
        throw new Error('lab session not found');
    }

    if (labSession.teacher_account_id != authenticated.account.id) {
        throw new Forbidden("le compte n'est pas associé à cette session TP");
    }

    return labSession;
}

export function setLabSessionNotesForStudents(id: string, notes: string | null, authenticated: AuthenticatedAccount) {
    const labSession = getLabSession(id, authenticated);

    if (labSession.teacher_account_id != authenticated.account.id) {
        throw new Forbidden()
    }

    const result = database.prepare(`
        UPDATE lab_sessions
        SET notes_for_students = ?
        WHERE id = ?
    `).run(notes, id);

    if (result.changes != 1) {
        throw new Error(`expected 1 affected row, got ${result.changes}`);
    }
}


export function createLabSessionJoinCode(id: string, joinableUntil: DateTime, authenticated: AuthenticatedAccount) {
    const labSession = getLabSession(id, authenticated);

    // this should not happen because the getLabSession call should have failed
    // but it's more robust with this added check
    if (labSession.teacher_account_id != authenticated.account.id) {
        throw new Forbidden("le compte n'est pas associé à cette session TP");
    }

    const code = randomHex(3);

    const result = database.prepare(`
        UPDATE lab_sessions
        SET join_code = ?, joinable_until = ?
        WHERE id = ?
    `).run(code, joinableUntil.toISO(), id);

    if (result.changes != 1) {
        throw new Error(`expected 1 affected row, got ${result.changes}`);
    }
}

export function deleteLabSessionJoinCode(id: string, authenticated: AuthenticatedAccount) {
    const labSession = getLabSession(id, authenticated);

    // this should not happen because the getLabSession call should have failed
    // but it's more robust with this added check
    if (labSession.teacher_account_id != authenticated.account.id) {
        throw new Forbidden("le compte n'est pas associé à cette session TP");
    }

    const result = database.prepare(`
        UPDATE lab_sessions
        SET join_code = null, joinable_until = null
        WHERE id = ?
    `).run(id);

    if (result.changes != 1) {
        throw new Error(`expected 1 affected row, got ${result.changes}`);
    }
}

export function getLabSessionByJoinCode(code: string): LabSession | undefined {
    return database.prepare(`
        SELECT
            id, title, created_at,
            join_code, joinable_until,
            notes_for_teacher, notes_for_students
        FROM lab_sessions
        WHERE join_code = ?
    `).get(code) as LabSession | undefined;
}

export function deleteLabSession(id: string, authenticated: AuthenticatedAccount) {
    const labSession = getLabSession(id, authenticated);

    // this should not happen because the getLabSession call should have failed
    // but it's more robust with this added check
    if (labSession.teacher_account_id != authenticated.account.id) {
        throw new Forbidden("le compte n'est pas associé à cette session TP");
    }

    const result = database.prepare(`
        DELETE FROM lab_sessions
        WHERE id = ?
    `).run(id);

    if (result.changes != 1) {
        throw new Error(`expected 1 row affected, got ${result.changes}`);
    }
}

export function createStudentLabSession(joinCode: string): string {
    const labSession = getLabSessionByJoinCode(joinCode);

    if (!labSession) {
        throw new Forbidden();
    }

    const now = new Date();
    const joinableUntil = new Date(labSession.joinable_until as string);
    if (now.getMilliseconds > joinableUntil.getMilliseconds) {
        throw new Forbidden('code expiré')
    }

    const id = randomHex(8);
    const createdAt = (new Date()).toISOString();
    const cookie = randomBase64(16);

    database.prepare(`
        INSERT INTO lab_student_sessions(
            id, created_at, lab_session_id,
            cookie
        ) VALUES (?, ?, ?, ?)
    `).run(id, createdAt, labSession.id, cookie);

    return cookie;
}

export function getStudentSessionsByLab(labSessionId: string, authenticated: AuthenticatedAccount) {
    // just for access control
    getLabSession(labSessionId, authenticated);

    return database.prepare(`
        SELECT id, created_at, lab_session_id, last_saved_at
        FROM lab_student_sessions
        WHERE lab_session_id = ?
    `).all(labSessionId) as LabStudentSession[];
}

export function getStudentLabSession(id: string, authenticated: AuthenticatedAccount) {
    // XXX why are we doing a JOIN here again?
    const studentLabSession = database.prepare(`
        SELECT s.id, s.created_at, lab_session_id, last_saved_at, teacher_account_id
        FROM lab_student_sessions s JOIN lab_sessions l ON s.lab_session_id = l.id
        WHERE s.id = ?
    `).get(id) as (LabStudentSession & {teacher_account_id: string}) | undefined;

    if (!studentLabSession) {
        throw new Error(`cannot find student lab session`);
    }

    if (studentLabSession.teacher_account_id == authenticated.account.id) {
        return studentLabSession;
    } else {
        throw new Forbidden();
    }
}

export function getStudentLabSessionSave(id: string, authenticated: AuthenticatedAccount) {
    // just for access control
    // XXX inneficient (we probably already retrieved the student lab session)
    getStudentLabSession(id, authenticated);
    
    return (database.prepare(
        `SELECT last_save FROM lab_student_sessions WHERE id = ?`
    ).get(id) as any).last_save as string;
}

/**
 * XXX should be renamed
 * because it returns the student sessions
 * AND their saves
 */
export function getLabSessionSaves(labSessionId: string, authenticated: AuthenticatedAccount) {
    // just for access control (and checking existence)
    // XXX not ideal
    getLabSession(labSessionId, authenticated);

    return database.prepare(`
        SELECT id, created_at, lab_session_id, last_save, last_saved_at
        FROM lab_student_sessions
        WHERE lab_session_id = ?
    `).all(labSessionId) as (LabStudentSession & { last_save: string })[];
}

/**
 * 
 * @returns the lab session Id this student session had
 */
export function deleteStudentLabSession(id: string, authenticated: AuthenticatedAccount) {
    const session = getStudentLabSession(id, authenticated);

    // XXX don't just rely on "getStudentLabSession" for access control

    const result = database.prepare(
        `DELETE FROM lab_student_sessions WHERE id = ?`
    ).run(id);

    if (result.changes != 1) {
        throw new Error(`expected 1 affected row, got ${result.changes}`);
    }

    return session.lab_session_id;
}

export class AccountExistsAlready extends Error {
    constructor() {
        super('un compte existe déjà avec cet email');

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, AccountExistsAlready.prototype);
    }
}

export async function createAccount(email: string, password: string, info?: string): Promise<string> {
    const id = randomHex(8);
    const createdAt = new Date().toISOString();

    const passwordHash = await argon2.hash(password);

    try {
        database.transaction(() => {
            database.prepare(`
                INSERT into accounts(id, email, created_at, info)
                VALUES (?, ?, ?, ?)
            `).run(id, email, createdAt, info || '');

            database.prepare(`
                INSERT into account_passwords(account_id, hash)
                VALUES (?, ?)
            `).run(id, passwordHash);
        })();
    } catch (error) {
        if (
            error instanceof BetterSQLite3.SqliteError
            && error.code === "SQLITE_CONSTRAINT_UNIQUE"
        ) {
            throw new AccountExistsAlready();
        }

        throw error;
    }

    return id;
}

export function createSessionCookie(accountId: string): string {
    const cookie = randomBase64(16);
    const createdAt = new Date().toISOString();

    database.prepare(`
        INSERT INTO cookies (value, account_id, created_at)
        VALUES (?, ?, ?)
    `).run(cookie, accountId, createdAt);

    return cookie;
}

export class LoginFailed extends Error {
    constructor() {
        super('login failed');

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, LoginFailed.prototype);
    }
}

export class WrongPassword extends LoginFailed {
    constructor() {
        super();

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, WrongPassword.prototype);
    }
}

export class NoAccountForThisEmail extends LoginFailed {
    email: string

    constructor(email: string) {
        super();

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, NoAccountForThisEmail.prototype);

        this.email = email;
    }
}

export async function login(email: string, password: string): Promise<string> {
    const account = database.prepare(`
        SELECT id FROM accounts WHERE email = ?
    `).get(email) as { id: string } | undefined;

    if (!account) {
        throw new NoAccountForThisEmail(email);
    }

    const accountPassword = database.prepare(`
        SELECT account_id, hash FROM account_passwords
        WHERE account_id = ?
    `).get(account.id) as { account_id: string, hash: string } | undefined;

    if (!accountPassword) {
        throw new Error(`no password for account "${account.id}" (email "${email}")`);
    }

    if (!await argon2.verify(accountPassword.hash, password)) {
        throw new WrongPassword();
    }

    return createSessionCookie(account.id);
}

export function createAccountCreationToken(info?: string): string {
    const token = randomHex(8);
    const createdAt = new Date().toISOString();

    database.prepare(`
        INSERT INTO account_creation_tokens (value, created_at, info)
        VALUES (?, ?, ?)
    `).run(token, createdAt, info);

    return token;
}

export function getAccountCreationToken(token: string) {
    return database.prepare(`
        SELECT value, created_at, info
        FROM account_creation_tokens
        WHERE value = ?
    `).get(token) as AccountCreationToken | undefined;
}

export function deleteAccountCreationToken(token: string) {
    const info = database.prepare(`
        DELETE FROM account_creation_tokens
        WHERE value = ?
    `).run(token);

    if (info.changes != 1) {
        throw new Error(`Expected 1 affected row, got ${info.changes}`);
    }

    return;
}

export class InvalidCookie extends Error {
    reason: string

    constructor(reason: string) {
        super();

        // Set the prototype explicitly.
        // See https://github.com/Microsoft/TypeScript-wiki/blob/main/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        Object.setPrototypeOf(this, InvalidCookie.prototype);

        this.reason = reason;
    }
}

export class AuthenticatedAccount {
    account: Account

    constructor(account: Account) {
        this.account = account;
    }
}

export class AuthenticatedLabStudentSession {
    session: LabStudentSession

    constructor(session: LabStudentSession) {
        this.session = session;
    }
}

type Cookie = {
    value: string
    account_id: string
    created_at: string
}

function getCookie(value: string) {
    return database.prepare(`
        SELECT value, account_id, created_at
        FROM cookies WHERE value = ?
    `).get(value) as Cookie | undefined;
}

// max cookie age in milliseconds
const maxCookieAge = 10*24*3600*1000;

export function getAccountFromCookie(cookie: string) {
    const dbCookie = getCookie(cookie);

    if (!dbCookie) {
        throw new InvalidCookie('cookie not in database');
    }

    const createdAt = new Date(dbCookie.created_at);
    const now = new Date();

    if ((now.getTime() - createdAt.getTime()) > maxCookieAge) {
        throw new InvalidCookie('cookie too old');
    }

    const account = database.prepare(`
        SELECT id, email, created_at FROM accounts
        WHERE id = ?
    `).get(dbCookie.account_id) as Account | undefined;

    if (!account) {
        throw new Error(`cannot find account with id ${dbCookie.account_id}`);
    }

    return new AuthenticatedAccount(account);
}

// TODO make it variable
const maxLabStudentSessionAge = 3*3600*1000

export function getLabStudentSessionFromCookie(cookie: string) {
    const labStudentSession = database.prepare(`
        SELECT id, created_at, lab_session_id, last_saved_at
        FROM lab_student_sessions
        WHERE cookie = ?
    `).get(cookie) as LabStudentSession | undefined;

    if (!labStudentSession) {
        throw new InvalidCookie('cookie not in database');
    }

    const createdAt = new Date(labStudentSession.created_at);
    const now = new Date();

    if ((now.getTime() - createdAt.getTime()) > maxLabStudentSessionAge) {
        throw new InvalidCookie('cookie too old');
    }

    return new AuthenticatedLabStudentSession(labStudentSession);
}

export function saveLabStudentSession(sessionId: string, source: string) {
    const now = new Date();

    const info = database.prepare(`
        UPDATE lab_student_sessions
        SET last_save = ?, last_saved_at = ?
        WHERE id = ?
    `).run(source, now.toISOString(), sessionId);

    if (info.changes != 1) {
        throw new Error(`expected 1 row changed, got ${info.changes}`);
    }
}

export function deleteCookie(cookie: string, authenticated: AuthenticatedAccount) {
    const dbCookie = getCookie(cookie);

    if (!dbCookie) {
        throw new Error('cookie not found in DB');
    }

    if (dbCookie.account_id != authenticated.account.id) {
        throw new Forbidden();
    }

    const info = database.prepare(`
        DELETE from cookies
        WHERE value = ?
    `).run(cookie);

    if (info.changes != 1) {
        throw new Error(`expected 1 changed row, got ${info.changes}`);
    }
}