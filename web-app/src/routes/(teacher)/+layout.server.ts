import type { LayoutServerLoad } from './$types';

export const load = (async ({ locals: { account }}) => {
    return {
        loggedInAs: account.account.email,
    };
}) satisfies LayoutServerLoad;