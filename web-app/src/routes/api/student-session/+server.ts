import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ locals: { labStudentSession }}) => {
    return json(labStudentSession.session.id);
};