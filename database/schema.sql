CREATE TABLE account_creation_tokens (
  value TEXT PRIMARY KEY,
  created_at TEXT,
  info TEXT
);

CREATE TABLE accounts (
  id TEXT PRIMARY KEY,
  email TEXT UNIQUE,
  created_at TEXT,
  info TEXT
);

CREATE TABLE account_passwords (
  account_id PRIMARY KEY UNIQUE REFERENCES accounts(id) ON DELETE CASCADE,
  hash TEXT
);

CREATE TABLE cookies (
  value TEXT PRIMARY KEY,
  account_id REFERENCES accounts(id) ON DELETE CASCADE,
  created_at TEXT
);

CREATE TABLE lab_sessions (
  id TEXT PRIMARY KEY,
  title TEXT,
  created_at TEXT,
  teacher_account_id REFERENCES accounts(id),
  join_code TEXT,
  joinable_until TEXT,
  notes_for_teacher TEXT,
  notes_for_students TEXT
);

CREATE TABLE lab_student_sessions (
  id TEXT PRIMARY KEY,
  created_at TEXT,
  lab_session_id REFERENCES lab_sessions(id) ON DELETE CASCADE,
  cookie TEXT,
  last_save TEXT,
  last_saved_at TEXT
);
